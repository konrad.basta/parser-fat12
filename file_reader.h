//
// Created by Konrad Basta on 16.12.2020.
//

#ifndef PROJECT1_FILE_READER_H
#define PROJECT1_FILE_READER_H

#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>

#define BY_PER_SEC 512 //bytes per sector

#define ENT_READ_ONLY(byte) ((byte & 0x01) ? 1 : 0)
#define ENT_HIDDEN(byte)    ((byte & 0x02) ? 1 : 0)
#define ENT_SYSTEM(byte)    ((byte & 0x04) ? 1 : 0)
#define ENT_VOLUME(byte)    ((byte & 0x08) ? 1 : 0)
#define ENT_DIRECTORY(byte) ((byte & 0x10) ? 1 : 0)
#define ENT_ARCHIVED(byte)  ((byte & 0x20) ? 1 : 0)
#define ENT_LFN(byte)       ((byte & 0x0F) ? 1 : 0)

#define DIRECTORY 0
#define VOLUME    1
#define UNDEFINED 2

#define CHARACTER_SET "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM.\\"

typedef uint64_t byte_t;
typedef uint64_t lba_t;
typedef uint16_t cluster_t;
typedef uint8_t  bool;

typedef struct boot_sector_t        boot_sector_t;
typedef struct directory_entry_t    directory_entry_t;
typedef struct my_time_t            my_time_t;
typedef struct date_t               date_t;

typedef struct disk_t               disk_t;
typedef struct volume_t             volume_t;
typedef struct dir_entry_t          dir_entry_t;
typedef struct dir_t                dir_t;
typedef struct file_t               file_t;

typedef struct bytes3_t             bytes3_t;       //3 bytes
typedef struct shorts2_t            shorts2_t;      //2 shorts
typedef union  map_t                map_t;          //fat12 to fat16

struct boot_sector_t {
    uint8_t     jump_code[3];   //jump_code
    char        oem_name[8];    //oem_name
    uint16_t    by_per_sec;     //bytes_per_sector
    uint8_t     sec_per_clu;    //sectors_per_cluster
    uint16_t    res_sec;        //reserved_sectors
    uint8_t     fat_count;      //fat_count
    uint16_t    root_dir_cap;   //root_directory_capacity
    uint16_t    log_sec16;      //logical_sectors16
    uint8_t     media_type;     //media_type
    uint16_t    sec_per_fat;    //sectors_per_fat
    uint16_t    sec_per_track;  //chs_sectors_per_tracks
    uint16_t    tracks_per_cyl; //chs_tracks_per_cylinder
    uint32_t    hid_sec;        //hidden_sectors
    uint32_t    log_sec32;      //logical_sectors32
    uint8_t     media_id;       //media_id
    uint8_t     head;           //chs_head
    uint8_t     ext_bpb_sign;   //ext_bpb_signature
    uint32_t    serial_number;  //serial_number
    char        vol_label[11];  //volume_label
    char        fsid[8];        //fsid
    uint8_t     boot_code[448]; //boot_code
    uint16_t    magic; //0xAA55 //magic
}__attribute__((packed));

struct my_time_t {
    uint16_t second:5;
    uint16_t minute:6;
    uint16_t hour:5;
};

struct date_t {
    uint16_t day:5;
    uint16_t month:4;
    uint16_t year:7;
};

struct directory_entry_t {
    u_char      filename[8];
    u_char      filename_extension[3];
    u_char      file_attributes;
    u_char      reserved;
    u_char      creation_time_ms;
    my_time_t   creation_time;
    date_t      creation_date;
    date_t      last_access_time;
    uint16_t    first_cluster_number_old;
    my_time_t   last_modification_time;
    date_t      last_modification_date;
    uint16_t    first_cluster_number_young;
    uint32_t    file_length;    // 0 dla katalogów
}__attribute__((packed));

struct disk_t {
    FILE* ptr;
};

struct volume_t {
    disk_t      *disk;
    lba_t       vol_start;      //volume_start
    lba_t       fat1_start;
    lba_t       fat2_start;
    lba_t       root_start;
    lba_t       data_start;
    lba_t       sec_per_clu;    //sectors_per_cluster
    byte_t      by_per_sec;     //bytes per sector
    byte_t      by_per_clu;     //bytes per cluster
    lba_t       sec_per_root;   //sectors_per_root
    cluster_t   avail_clu;      //available_clusters
    byte_t      avail_by;       //available_bytes
    uint32_t    fat_entry_count;
    uint16_t    *fat_data;
};

struct dir_entry_t {
    char    name[13];       // nazwa pliku/katalogu (w przypadku pliku nazwa ma być z kropką i rozszerzeniem), bez nadmiarowych spacji i zakończona terminatorem,
    byte_t  size;           // rozmiar; pliku/katalogu w bajtach,
    bool    is_archived;    // wartość atrybutu: plik zarchiwizowany (0 lub 1),
    bool    is_readonly;    // wartość atrybutu: plik tylko do odczytu (0 lub 1),
    bool    is_system;      // wartość atrybutu: plik jest systemowy (0 lub 1),
    bool    is_hidden;      // wartość atrybutu: plik jest ukryty (0 lub 1),
    bool    is_directory;   // wartość atrybutu: katalog (0 lub 1).
    bool    has_long_name;
    const char *  long_name;
};

struct dir_t {
    volume_t    *vol;           //volume
    size_t      cur;            //current entry
    size_t      num_of_ent;     //number of entries
    dir_entry_t *ent_chain;     //entries chain
};

struct file_t {
    volume_t        *vol;       //dir_entry_t     *entry;
    byte_t          size;
    byte_t          cur;        //current
    cluster_t       num_of_clu; //number of clusters
    cluster_t       *clu_chain; //clusters chain
};

struct bytes3_t {
    uint8_t b0;
    uint8_t b1;
    uint8_t b2;
};

struct shorts2_t {
    uint16_t c1:12;
    uint16_t c2:12;
}__attribute__((packed));

union map_t {           //fat12 to fat16
    bytes3_t bytes3;
    shorts2_t short2;
};

struct disk_t* disk_open_from_file(const char* volume_file_name);
int disk_read(struct disk_t* pdisk, int32_t first_sector, void* buffer, int32_t sectors_to_read);
int disk_close(struct disk_t* pdisk);

struct volume_t* fat_open(struct disk_t* pdisk, uint32_t first_sector);
int fat_close(struct volume_t* pvolume);

struct file_t* file_open(struct volume_t* pvolume, const char* file_name);
int file_close(struct file_t* stream);
size_t file_read(void *ptr, size_t size, size_t nmemb, struct file_t *stream);
int32_t file_seek(struct file_t* stream, int32_t offset, int whence);

struct dir_t* dir_open(struct volume_t* pvolume, const char* dir_path);
int dir_read(struct dir_t* pdir, struct dir_entry_t* pentry);
int dir_close(struct dir_t* pdir);

void my_free(void *p, ...);
void create_path(char* dest, const char* src);
void create_entry_name(char* filename, const directory_entry_t src);
void copy_entry(directory_entry_t* dest, directory_entry_t* src);

cluster_t search_root(struct volume_t* pvolume, char* filename, int* type, directory_entry_t* entry);
cluster_t search_directory(struct volume_t* pvolume, cluster_t dir_start, char* filename, int* type, directory_entry_t* entry);
int read_directory_entry(struct volume_t* pvolume, cluster_t clu_start, dir_t* dir);
int read_root_entry(struct volume_t* pvolume, dir_t* dir);


#endif //PROJECT1_FILE_READER_H

/************************
typedef struct entry_t {
    dir_t   *dir;
    char    name[13];
    byte_t  size;
    uint8_t is_archived;
    uint8_t is_readonly;
    uint8_t is_system;
    uint8_t is_hidden;
    uint8_t is_directory;
    struct {
        uint8_t  day;
        uint8_t  month;
        uint16_t year;
    }creation_date;
    struct {
        uint8_t  hour;
        uint8_t  minute;
        uint8_t  second;
    }creation_time;
} entry_t;
************************/
