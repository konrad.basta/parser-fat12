//
// Created by Konrad Basta on 30.11.2020.
//

#define TD
//#define TF

#include "file_reader.h"
void perror(const char* s);
#include <errno.h>
const char * const sys_errlist[];
int sys_nerr;
int errno;
//#include <z3.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <ctype.h>

#include "tested_declarations.h"
#include "rdebug.h"
#include "tested_declarations.h"
#include "rdebug.h"

struct disk_t* disk_open_from_file(const char* volume_file_name) {
    if (!volume_file_name) {
        perror("EFAULT");
        return NULL;
    }
    FILE *fp = fopen (volume_file_name,"rb");
    if (!fp) {
        perror("ENOENT");
        return NULL;
    }
    disk_t *pdisk = (disk_t *) malloc (sizeof(disk_t));     //pointer to the disk
    if (!pdisk) {
        fclose (fp);
        perror ("ENOMEM");
        return NULL;
    }
    pdisk->ptr = fp;
    return pdisk;
}

int disk_read(struct disk_t* pdisk, int32_t first_sector, void* buffer, int32_t sectors_to_read) {
    if (!pdisk||!pdisk->ptr||first_sector<0||!buffer||sectors_to_read<1) {
        perror ("EFAULT");
        return -1;
    }
    if (fseek (pdisk->ptr, (first_sector+sectors_to_read) * BY_PER_SEC, SEEK_SET)) {
        perror ("ERANGE");
        return -1;
    }
    fseek  (pdisk->ptr, first_sector * BY_PER_SEC, SEEK_SET);
    return fread (buffer, BY_PER_SEC, sectors_to_read, pdisk->ptr);
}

int disk_close(struct disk_t* pdisk) {
    if (!pdisk) {
        perror ("EFAULT");
        return -1;
    }
    if (!pdisk->ptr) {
        free (pdisk);
        perror ("EFAULT");
        return -1;
    }
    fclose (pdisk->ptr);
    free (pdisk);
    return 0;
}

struct volume_t* fat_open(struct disk_t* pdisk, uint32_t first_sector) {
    if (!pdisk||!pdisk->ptr) {
        perror ("EFAULT");
        return NULL;
    }
    boot_sector_t bs;   //boot_sector
    volume_t *pvol = (volume_t*) malloc (sizeof (volume_t)); //pointer to the volume
    if (!pvol) {
        perror ("ENOMEM");
        return NULL;
    }
    int result;
    result = disk_read (pdisk, first_sector, &bs, 1);
    if (result == -1) {
        free (pvol);
        return NULL;
    }
    // walidacje boot sektora
    switch (bs.sec_per_clu) {
        case 1:
        case 2:
        case 4:
        case 8:
        case 16:
        case 32:
        case 64:
        case 128:
            break;
        default:
            perror ("EINVAL");
            free (pvol);
            return NULL;
    }

    if (    (bs.res_sec == 0)
        ||  ((bs.fat_count != 1 && bs.fat_count != 2))
        ||  ((bs.root_dir_cap * sizeof(struct directory_entry_t)) % bs.by_per_sec != 0)
        ||  (!(bs.log_sec32 == 0 ^ bs.log_sec16 == 0))
        ||  ((bs.log_sec16 == 0 && bs.log_sec32 < 65535))
        ||  (bs.magic != 0xAA55)) {
        perror ("EINVAL");
        free (pvol);
        return NULL;
    }
    //
    pvol->disk            = pdisk;
    pvol->vol_start       = 0;
    pvol->fat1_start      = pvol->vol_start + bs.res_sec;
    pvol->fat2_start      = pvol->fat1_start + bs.sec_per_fat;
    pvol->root_start      = pvol->fat1_start + bs.fat_count * bs.sec_per_fat;
    pvol->sec_per_clu     = bs.sec_per_clu;
    pvol->by_per_sec      = bs.by_per_sec;
    pvol->by_per_clu      = bs.sec_per_clu * bs.by_per_sec;
    pvol->sec_per_root    = bs.root_dir_cap * sizeof (directory_entry_t) / bs.by_per_sec;
    if (bs.root_dir_cap * sizeof (directory_entry_t) % bs.by_per_sec != 0)
        pvol->sec_per_root += 1;
    pvol->data_start      = pvol->root_start + pvol->sec_per_root;
    uint16_t log_sec      = bs.log_sec16 ? bs.log_sec16 : bs.log_sec32;
    pvol->avail_clu       = (log_sec - bs.res_sec - bs.fat_count * bs.sec_per_fat - pvol->sec_per_root) / bs.sec_per_clu;
    pvol->avail_by        = pvol->avail_clu * bs.sec_per_clu * bs.by_per_sec;
    pvol->fat_entry_count = pvol->avail_clu + 2;

    size_t by_per_fat = bs.sec_per_fat * bs.by_per_sec; //bytes_per_fat
    uint8_t *fat1 = malloc (by_per_fat);
    uint8_t *fat2 = malloc (by_per_fat);
    if (!fat1 || !fat2) {
        perror ("ENOMEM");
        my_free (fat1, fat2, pvol, NULL);
        return NULL;
    }

    int res  = disk_read (pdisk, pvol->fat1_start, fat1, bs.sec_per_fat);
    int res2 = disk_read (pdisk, pvol->fat2_start, fat2, bs.sec_per_fat);
    if (res!=bs.sec_per_fat || res2!=bs.sec_per_fat) {
        my_free (fat1, fat2, pvol, NULL);
        return NULL;
    }

    if (memcmp (fat1,fat2,bs.sec_per_fat) != 0) {
        perror ("EINVAL");
        my_free (fat1, fat2, pvol, NULL);
        return NULL;
    }

    pvol->fat_data = (uint16_t*) malloc (sizeof (uint16_t) * pvol->avail_clu);
    if (!pvol->fat_data) {
        perror ("ENOMEM");
        my_free (fat1, fat2, pvol, NULL);
        return NULL;
    }

    map_t map;
    for (lba_t i = 0, j = 0; j < pvol->avail_clu; ++i,++j) {
        map.bytes3.b0 = fat1[i];
        map.bytes3.b1 = fat1[++i];
        map.bytes3.b2 = fat1[++i];

        pvol->fat_data[j]   = map.short2.c1;
        pvol->fat_data[++j] = map.short2.c2;
    }

    my_free (fat1, fat2, NULL);

    return pvol;
}

int fat_close(struct volume_t* pvolume) {
    if (!pvolume) {
        perror ("EFAULT");
        return -1;
    }
    if (pvolume->fat_data) {
        free (pvolume->fat_data);
    }
    free (pvolume);
    return 0;
}

struct file_t* file_open(struct volume_t* pvolume, const char* file_name) {
    if (!pvolume || !file_name) {
        perror ("EFAULT");
        return NULL;
    }
    if (strspn (file_name, CHARACTER_SET) != strlen (file_name)) {
        perror ("ENOENT");
        return NULL;
    }

    char  *path = (char*)  malloc (strlen(file_name) + 1);
    if (!path) {
        perror ("ENOMEM");
        return NULL;
    }
    create_path (path, file_name);
    char* fname = strrchr (path, '\\');
    cluster_t dir_start;
    int type;
    directory_entry_t entry;
    if (fname) {
        char* search_dir = strtok (path,"\\");
        dir_start = search_root (pvolume, search_dir, &type, &entry);
        if (!dir_start || type != DIRECTORY) {
            free (path);
            perror ("ENOENT");
            return NULL;
        }
        search_dir = strtok (NULL, "\\");
        while (search_dir) {
            if (type != DIRECTORY) {
                free (path);
                perror ("ENOENT");
                return NULL;
            }
            dir_start = search_directory (pvolume, dir_start, search_dir, &type, &entry);
            if (!dir_start) {
                free (path);
                perror ("ENOENT");
                return NULL;
            }
            search_dir = strtok (NULL, "\\");
        }
    } else {
        dir_start = search_root (pvolume, path, &type, &entry);
        if (!dir_start) {
            free(path);
            perror ("ENOENT");
            return NULL;
        }
    }
    free (path);

    if (type == DIRECTORY) {
        perror ("ENOENT");
        return NULL;
    }
    file_t *pfile = (file_t*) malloc (sizeof(file_t));  //pointer to the file
    if (!pfile) {
        perror ("ENOMEM");
        return NULL;
    }

    pfile->vol  = pvolume;
    pfile->size = entry.file_length;
    pfile->cur  = 0;

    cluster_t cluster = entry.first_cluster_number_young; // fat32 -> (buffer[i].first_cluster_number_old<<16 | buffer[i].first_cluster_number_young);
    for (pfile->num_of_clu = 1; cluster != pvolume->fat_data[0]; ++pfile->num_of_clu) {
        cluster = pvolume->fat_data[cluster];
    }

    pfile->clu_chain = (cluster_t*) malloc (sizeof(cluster_t) * pfile->num_of_clu);
    if (!pfile->clu_chain) {
        free (pfile);
        perror ("ENOMEM");
        return NULL;
    }

    pfile->clu_chain[0] = entry.first_cluster_number_young;
    for (cluster_t clu_count = 1; pfile->clu_chain[clu_count - 1] != pfile->vol->fat_data[0]; ++clu_count) {
        pfile->clu_chain[clu_count] = pfile->vol->fat_data[pfile->clu_chain[clu_count - 1]];
    }

    return pfile;
}

int file_close(struct file_t* stream) {
    if (!stream) {
        perror ("EFAULT");
        return -1;
    }
    my_free(stream->clu_chain, stream, NULL);
    return 0;
}

size_t file_read(void *ptr, size_t size, size_t nmemb, struct file_t *stream) {
    //WALIDACJA
    if (!ptr || !size || !nmemb || !stream) {
        perror ("EFAULT");
        return -1;
    }
    //NIE RÓB NIC JEŻELI JUŻ JESTEŚ NA KOŃCU PLIKU
    if (stream->cur == stream->size)
        return 0;
    //USTAWIENIE ZMIENNYCH POCZĄTKOWYCH
    const lba_t  sec_per_clu      = stream->vol->sec_per_clu;               //sectors per cluster
    byte_t       cur_by           = stream->cur % BY_PER_SEC;               //current byte
    lba_t        cur_sec          = stream->cur / BY_PER_SEC % sec_per_clu; //current sector
    cluster_t    cur_clu          = stream->cur / BY_PER_SEC / sec_per_clu; //current cluster
    byte_t       read_by          = 0;                                      //read bytes
    lba_t        sec_to_be_read   = stream->vol->data_start + (stream->clu_chain[cur_clu] - 2) * sec_per_clu + cur_sec;             //sector to be read
    byte_t       by_to_read       = ((stream->cur + size * nmemb) > stream->size) ? (stream->size - stream->cur) : (size * nmemb);  //bytes to read
    const byte_t by_at_beg        = BY_PER_SEC - cur_by < by_to_read ? BY_PER_SEC - cur_by : by_to_read;                            //bytes at the beginning
    const byte_t by_at_the_end    = ((by_to_read - by_at_beg) % BY_PER_SEC) ? ((by_to_read - by_at_beg) % BY_PER_SEC) : 0;          //bytes at the end
    const lba_t  full_sec_to_read = (by_to_read - by_at_beg) / BY_PER_SEC;                                                          //full sectors to read
    uint8_t      buffer[BY_PER_SEC];
    //CZYTANIE DO KOŃCA SEKTORA
    int res = disk_read (stream->vol->disk, sec_to_be_read, buffer, 1);
    if (res == -1) {
        perror ("ERANGE");
        return -1;
    }
    mempcpy (ptr, &buffer[cur_by], by_at_beg);
    stream->cur += by_at_beg;
    cur_by      += by_at_beg;
    by_to_read  -= by_at_beg;
    read_by     += by_at_beg;
    //assert (stream->cur <= stream->size);
    assert (cur_by <= BY_PER_SEC);
    if (by_to_read) {
        cur_by = 0;
        ++cur_sec;
        if (cur_sec == sec_per_clu) {
            cur_sec = 0;
            ++cur_clu;
            if (cur_clu == stream->num_of_clu) {
                return read_by/size;
            }
        }
    } else {
        return read_by/size;;
    }
    sec_to_be_read = stream->vol->data_start+(stream->clu_chain[cur_clu]-2)*sec_per_clu+cur_sec;
    //CZYTANIE PEŁNYCH SEKTORÓW
    for (lba_t k = 0; k < full_sec_to_read; ++k) {
        res = disk_read (stream->vol->disk, sec_to_be_read, (uint8_t*)ptr + read_by, 1);
        if (res == -1) {
            perror ("ERANGE");
            return -1;
        }
        stream->cur += BY_PER_SEC;
        by_to_read  -= BY_PER_SEC;
        read_by     += BY_PER_SEC;
        ++cur_sec;
        if (cur_sec == sec_per_clu) {
            cur_sec = 0;
            ++cur_clu;
            if (cur_clu == stream->num_of_clu) {
                return read_by/size;;
            }
        }
        sec_to_be_read = stream->vol->data_start + (stream->clu_chain[cur_clu] - 2) * sec_per_clu + cur_sec;
    }
    //CZYTANIE RESZTY W OSTATNIM SEKTORZE
    res = disk_read (stream->vol->disk, sec_to_be_read, buffer, 1);
    if (res == -1) {
        perror ("ERANGE");
        return -1;
    }
    mempcpy ((uint8_t *)ptr+read_by, &buffer[0], by_at_the_end);
    stream->cur += by_at_the_end;
    read_by     += by_at_the_end;
    assert (stream->cur <= stream->size);
    //RETURN
    return read_by/size;
}

int32_t file_seek(struct file_t* stream, int32_t offset, int whence) {
    if (!stream) {
        perror ("EFAULT");
        return -1;
    }
    int32_t new_position;

    switch (whence) {
        case SEEK_SET:
            new_position = offset;
            break;
        case SEEK_CUR:
            new_position = stream->cur + offset;
            break;
        case SEEK_END:
            new_position = stream->size + offset;
            break;
        default:
            perror ("EINVAL");
            return -1;
    }

    if (new_position < 0 || new_position > (int32_t)stream->size) {
        perror ("ENXIO");
        return -1;
    }
    stream->cur = new_position;

    return stream->cur;
}

struct dir_t* dir_open(struct volume_t* pvolume, const char* dir_path) {
    if (!pvolume || !dir_path) {
        perror ("EFAULT");
        return NULL;
    }

    if (strspn (dir_path, CHARACTER_SET) != strlen (dir_path)) {
        perror ("ENOENT");
        return NULL;
    }

    char  *path = (char*)  malloc (strlen(dir_path) + 1);
    if (!path) {
        perror ("ENOMEM");
        return NULL;
    }
    create_path (path, dir_path);
    cluster_t dir_start;
    int type;
    directory_entry_t entry;
    int root = 0;
    if (*path != '\0') {
        char* search_dir = strtok (path,"\\");
        dir_start = search_root (pvolume, search_dir, &type, &entry);
        if (!dir_start || type != DIRECTORY) {
            free (path);
            perror ("ENOENT");
            return NULL;
        }
        search_dir = strtok (NULL, "\\");
        while (search_dir) {
            if (type != DIRECTORY) {
                free (path);
                perror ("ENOENT");
                return NULL;
            }
            dir_start = search_directory (pvolume, dir_start, search_dir, &type, &entry);
            if (!dir_start) {
                free (path);
                perror ("ENOENT");
                return NULL;
            }
            search_dir = strtok (NULL, "\\");
        }
    } else {
        type = DIRECTORY;
        root = 1;
    }
    free (path);

    if (type != DIRECTORY) {
        perror ("ENOENT");
        return NULL;
    }
    dir_t *pdir = (dir_t*) malloc (sizeof(dir_t));
    if (!pdir) {
        perror ("ENOMEM");
        return NULL;
    }

    pdir->vol = pvolume;
    pdir->cur = 0;
    int res;
    if (root) {
        res = read_root_entry (pvolume, pdir);
    } else {
        res = read_directory_entry (pvolume, dir_start, pdir);
    }
    if (res) {
        return NULL;
    }
    return pdir;
}

int dir_read(struct dir_t* pdir, struct dir_entry_t* pentry) {
    if (!pdir || !pentry) {
        perror ("EFAULT");
        return -1;
    }
    if (pdir->cur == pdir->num_of_ent) {
        perror ("ENXIO");
        return 1;
    }
    memcpy(pentry,pdir->ent_chain + pdir->cur,sizeof (dir_entry_t));
    ++pdir->cur;
    return 0;
}

int dir_close(struct dir_t* pdir) {
    if (!pdir) {
        perror ("EFAULT");
        return -1;
    }
    free(pdir->ent_chain);
    free(pdir);
    return 0;
}

void my_free(void *p, ...) {
    va_list ptr;
    va_start(ptr, p);
    //void *p;

    free(p);
    do {
        p = va_arg(ptr,void*);
        free(p);
    } while (p);

    va_end(ptr);
}

inline void create_path(char* dest, const char* src) {
    assert (dest != NULL && src != NULL);
    while (*src == '\\') {
        ++src;
    }
    while (*src) {
        *dest++ = toupper (*src++);
    }
    *dest = '\0';
}

void create_entry_name(char* filename, const directory_entry_t src) {
    assert(filename != NULL && src.filename[0] != '\0');
    int size = 0;
    for (; size < 8; ++size) {
        if (*(src.filename + size) == ' ')
            break;
        filename[size] = *(src.filename + size);
    }
    if (!ENT_DIRECTORY(src.file_attributes)) {
        if (*(src.filename_extension) != ' ')
            filename[size++] = '.';
        for (int j = 0; j < 3; ++j, ++size) {
            if (*(src.filename_extension + j) == ' ')
                break;
            filename[size] = *(src.filename_extension + j);
        }
    }
    filename[size] = '\0';
}

void copy_entry(directory_entry_t* dest, directory_entry_t* src) {
    for (int i = 0; i < 8; ++i) {
        dest->filename[i] = src->filename[i];
    }
    for (int i =0; i < 3; ++i) {
        dest->filename_extension[i] = src->filename_extension[i];
    }
    dest->file_attributes            = src->file_attributes;
    dest->reserved                   = src->reserved;
    dest->creation_time_ms           = src->creation_time_ms;
    dest->creation_time              = src->creation_time;
    dest->creation_date              = src->creation_date;
    dest->last_access_time           = src->last_access_time;
    dest->first_cluster_number_old   = src->first_cluster_number_old;
    dest->last_modification_time     = src->last_modification_time;
    dest->last_modification_date     = src->last_modification_date;
    dest->first_cluster_number_young = src->first_cluster_number_young;
    dest->file_length                = src->file_length;
}

cluster_t search_root(struct volume_t* pvolume, char* filename, int* type, directory_entry_t* entry) {
    if (!pvolume || !filename || !type) {
        return 0;
    }

    *type = UNDEFINED;

    directory_entry_t buffer[BY_PER_SEC / sizeof (struct directory_entry_t)];
    for (lba_t i = 0; i < pvolume->sec_per_root; ++i) {
        int res = disk_read (pvolume->disk, pvolume->root_start + i, &buffer, 1);
        if (res == -1) {
            return 0;
        }
        char name[13];
        for (size_t j = 0; j < BY_PER_SEC / sizeof (directory_entry_t); ++j) {
            if (buffer[j].filename[0] == '\0') {
                return 0;
            }
            create_entry_name (name, buffer[j]);
            if (!strcmp(name, filename)) {
                if (ENT_DIRECTORY(buffer[j].file_attributes)) {
                    *type = DIRECTORY;
                }
                /* else {
                    perror ("ENOENT");
                    return 0;
                }*/
                copy_entry (entry, &buffer[j]);
                return (cluster_t)buffer[j].first_cluster_number_young;
            }
        }
    }
    return 0;
}

cluster_t search_directory(struct volume_t* pvolume, cluster_t dir_start, char* filename, int* type, directory_entry_t* entry) {
    if (!pvolume || !filename || !type) {
        return 0;
    }

    directory_entry_t buffer[BY_PER_SEC / sizeof (struct directory_entry_t)];
    cluster_t cur_clu = dir_start;
    lba_t     cur_sec = pvolume->data_start + (cur_clu - 2) * pvolume->sec_per_clu;
    lba_t     sec_num = 0;
    *type = UNDEFINED;

    do {
        int res = disk_read (pvolume->disk, cur_sec, &buffer, 1);
        if (res == -1) {
            return 0;
        }
        char name[13];
        for (size_t i = 0; i < BY_PER_SEC / sizeof (directory_entry_t); ++i) {
            if (buffer[i].filename[0] == '\0') {
                return 0;
            }
            create_entry_name (name, buffer[i]);
            if (!strcmp(name, filename)) {
                if (ENT_DIRECTORY(buffer[i].file_attributes)) {
                    *type = DIRECTORY;
                }/* else if (ENT_VOLUME(buffer[i].file_attributes)) {
                    *type = VOLUME;
                } else {
                    perror ("ENOENT");
                    return 0;
                }*/
                copy_entry(entry, &buffer[i]);
                return (cluster_t)buffer[i].first_cluster_number_young;
            }
        }
        ++sec_num;
        if (sec_num == pvolume->sec_per_clu) {
            cur_clu = pvolume->fat_data[cur_clu];
            if (cur_clu == pvolume->fat_data[0]) {
                return 0;
            }
            cur_sec = pvolume->data_start + (cur_clu - 2) * pvolume->sec_per_clu;
            sec_num = 0;
        } else {
            ++cur_sec;
        }
    } while (1);
}

int read_directory_entry(struct volume_t* pvolume, cluster_t clu_start, dir_t* dir) {
    assert(pvolume && clu_start >= 2 && dir);

    directory_entry_t buffer[BY_PER_SEC / sizeof (directory_entry_t)];
    cluster_t cur_clu = clu_start;
    lba_t     cur_sec = pvolume->data_start + (cur_clu - 2) * pvolume->sec_per_clu;
    lba_t     sec_num = 0;
    dir->num_of_ent = 0;

    //Liczenie ilości wpisów
    int end = 0;
    do {
        int res = disk_read (pvolume->disk, cur_sec, buffer, 1);
        if (!res) {
            return 1;
        }

        for (size_t i = 0; i < BY_PER_SEC / sizeof (directory_entry_t); ++i) {
            if (buffer[i].filename[0] == '\0') {
                end = 1;
                break;
            }
            ++dir->num_of_ent;
        }
        ++sec_num;
        if (sec_num == pvolume->sec_per_clu) {
            cur_clu = pvolume->fat_data[cur_clu];
            if (cur_clu == pvolume->fat_data[0]) {
                break;
            }
            cur_sec = pvolume->data_start + (cur_clu - 2) * pvolume->sec_per_clu;
            sec_num = 0;
        } else {
            ++cur_sec;
        }
    } while (!end);

    dir->ent_chain = malloc(dir->num_of_ent * sizeof (dir_entry_t));
    if (!dir->ent_chain) {
        perror ("ENOMEM");
        return 1;
    }

    //Wypełnianie łańcucha wpisów
    cur_clu = clu_start;
    cur_sec = pvolume->data_start + (cur_clu - 2) * pvolume->sec_per_clu;
    sec_num = 0;
    int shift = 0;
    int j = 0;
    do {
        int res = disk_read (pvolume->disk, cur_sec, buffer, 1);
        if (!res) {
            return 1;
        }

        for (size_t i = 0; i < BY_PER_SEC / sizeof (directory_entry_t); ++i) {
            if (buffer[i].filename[0] == '\0') {
                return 0;
            }
            if (buffer[i].filename[0] > 127) {
                ++shift;
                --dir->num_of_ent;
                continue;
            }
            create_entry_name (dir->ent_chain[j].name, buffer[i]);
            dir->ent_chain[j].size         = buffer[i].file_length;
            dir->ent_chain[j].is_archived  = ENT_ARCHIVED(buffer[i].file_attributes);
            dir->ent_chain[j].is_readonly  = ENT_READ_ONLY(buffer[i].file_attributes);
            dir->ent_chain[j].is_system    = ENT_SYSTEM(buffer[i].file_attributes);
            dir->ent_chain[j].is_hidden    = ENT_HIDDEN(buffer[i].file_attributes);
            dir->ent_chain[j].is_directory = ENT_DIRECTORY(buffer[i].file_attributes);
            ++j;
        }
        ++sec_num;
        if (sec_num == pvolume->sec_per_clu) {
            cur_clu = pvolume->fat_data[cur_clu];
            if (cur_clu == pvolume->fat_data[0]) {
                return 0;
            }
            cur_sec = pvolume->data_start + (cur_clu - 2) * pvolume->sec_per_clu;
            sec_num = 0;
        } else {
            ++cur_sec;
        }
    } while (1);
}

int read_root_entry(struct volume_t* pvolume, dir_t* dir) {
    assert(pvolume && dir);

    directory_entry_t buffer[BY_PER_SEC / sizeof (directory_entry_t)];
    lba_t     cur_sec = pvolume->root_start;
    lba_t     sec_num = 0;
    dir->num_of_ent = 0;

    //Liczenie ilości wpisów
    int end = 0;
    do {
        int res = disk_read (pvolume->disk, cur_sec, buffer, 1);
        if (!res) {
            return 1;
        }

        for (size_t i = 0; i < BY_PER_SEC / sizeof (directory_entry_t); ++i) {
            if (buffer[i].filename[0] == '\0') {
                end = 1;
                break;
            }
            ++dir->num_of_ent;
        }
        ++sec_num;
        if (sec_num == pvolume->sec_per_root) {
            break;
        } else {
            ++cur_sec;
        }
    } while (!end);

    dir->ent_chain = malloc(dir->num_of_ent * sizeof (dir_entry_t));
    if (!dir->ent_chain) {
        perror ("ENOMEM");
        return 1;
    }

    //Wypełnianie łańcucha wpisów
    sec_num = 0;
    cur_sec = pvolume->root_start;
    int shift = 0;
    do {
        int res = disk_read (pvolume->disk, cur_sec, buffer, 1);
        if (!res) {
            return 1;
        }

        for (size_t i = 0; i < BY_PER_SEC / sizeof (directory_entry_t); ++i) {
            if (buffer[i].filename[0] == '\0') {
                return 0;
            }
            if (buffer[i].filename[0] > 127) {
                ++shift;
                --dir->num_of_ent;
                continue;
            }
            create_entry_name (dir->ent_chain[i-shift].name, buffer[i]);
            dir->ent_chain[i-shift].size         = buffer[i].file_length;
            dir->ent_chain[i-shift].is_archived  = ENT_ARCHIVED(buffer[i].file_attributes);
            dir->ent_chain[i-shift].is_readonly  = ENT_READ_ONLY(buffer[i].file_attributes);
            dir->ent_chain[i-shift].is_system    = ENT_SYSTEM(buffer[i].file_attributes);
            dir->ent_chain[i-shift].is_hidden    = ENT_HIDDEN(buffer[i].file_attributes);
            dir->ent_chain[i-shift].is_directory = ENT_DIRECTORY(buffer[i].file_attributes);
        }
        ++sec_num;
        if (sec_num == pvolume->sec_per_root) {
                return 0;
        } else {
            ++cur_sec;
        }
    } while (1);
}

